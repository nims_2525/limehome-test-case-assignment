QA Engineer Challenge


Task #1: Writing Test Scenarios

1. To verify valid login details
2. To verify whether the check-out date field accepts a later date than checkin date.
3. To check if error is reported if check-out date field is in the past
4. To verify that check-in- date and check-out- dates are the same as selected in booking confirmation page.
5. To check Hotel name, Location, room type, Total Day, price per night are same in checkin page as in the booking confirmation page.
6. To check whether the app accepts only correct reference no. And last name for retrieving the checkin info and do online checkin
7. To check for non german nationalities that it is mandatory or not to upload/take picture of the passport.
8. To  check whether the app is bilingual English/German.
9. To check whether if the app is set to English or German then all the words in the app are converted to the set language or not
10. To check whether the app opens properly in the mobile also
11. To check whether the application is not going out of screen in m obile in both potrait and landscape view
12. To check whether the look and feel of the app is same in mobile as well as desktop
13. To check whether the app is easy to use in the mobile in both the views potrait/Landscape
14. To check whether the user gets a success message after doing sucessfull online checkin
15. To check if user has internet or power failure while doing online checkin then he/she should be able to again do checkin after getting back internet and power.

Task #2: Finding Defects

1. The app is not directed to the homepage when clicked on the Limehome logo.
2. After clicking on the "DE" button the page is not translated to German language.
3. Last Name field is accepting special characters and numerical values.
4. Booking Reference field accepts input less than ten characters and special characters as well.
5. On booking details homepage, "Personal Details", "Trip", "Customer Support", "Passport" field description are written in gibberish.
6. No contact details are mentioned in the "Customer Support" field.
7. Translating the booking details page to German language does not translate all the displayed information to German.
8. Translating the booking details page to German language does not translate the input fields to German. e.g. "First Name", "Vorname".
9. Passport field is able to accept any random input characters.
10. Date of Birth field is able to accept future dates.


Task #3: Defect Reporting

summary- Numbers and special characters are being accepted in the Last name field

Description- Environment- QA 

Username -xxxxxxxx

Password-xxxxxxxxx

When the user tries to enter the special characters and Numbers in the last name field then he/she is able to do so.

Steps to reproduce:-
1. Login using the above credentials.
2. click on the last name field.
3. Enter special characters and Numbers

severity- High

priority- high

Upload screenshots

Reporter- xxxxxxx

Assignee-xxxxxxx

Status- New

Sprint no.- 10


Task #4: Automation (Bonus)

    import org.openqa.selenium.firefox.FirefoxDriver;
    import org.openqa.selenium.support.ui.WebDriverWait;
    import org.openqa.selenium.*;
   

    public class Hotel checkin {
   
    WebDriver driver = new FirefoxDriver();
        driver.navigate().to("Url");

        WebElement strvalue = driver.findElement(By.xpath("  "));
        String expected = "Text to compare";
        String actual = strvalue.getText();
        System.out.println(actual);

    if(expected.equals(actual)){
        System.out.println("Pass");
    }
        else {
            System.out.println("Fail");
        }
